/**
 * Copyright (C) 2015 INRIA, Université Lille 1
 *
 * Contacts: daniel.romero@inria.fr laurence.duchien@inria.fr & lionel.seinturier@inria.fr
 * Date: 09/2015
 
 * This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this 
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package fr.inria.paasage.saloon.price.model.lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;

import eu.paasage.camel.provider.Attribute;
import eu.paasage.camel.provider.Feature;
import eu.paasage.camel.provider.ProviderModel;
import eu.paasage.camel.type.EnumerateValue;
import fr.inria.paasage.saloon.price.api.IProviderPriceEstimator;
import fr.inria.paasage.saloon.price.model.tools.Constants;
import fr.inria.paasage.saloon.price.model.tools.ProviderModelTool;

public class GwdgPriceEstimator implements IProviderPriceEstimator 
{
	
	public static Logger logger= EstimatorsManager.logger;
	
	Map<String,Double> vmsMap;
	
	
	public GwdgPriceEstimator()
	{
		vmsMap= new Hashtable<String, Double>(); 
	}

	public double estimatePrice(ProviderModel fm) 
	{
		double price= 0; 
		Feature vm= ProviderModelTool.getFeatureByName(fm, ProviderModelTool.VIRTUAL_MACHINE_FEATURE); 
		
		if(vm==null)
		{
			vm= ProviderModelTool.getFeatureByName(fm, ProviderModelTool.VIRTUAL_MACHINE_FEATURE_ALT); 
		}
		
		if(vm!=null)
		{
			price=computeVmsPrice(vm); 
		}
		
		return price;
	}
	
	protected double computeVmsPrice(Feature vm)
	{
		logger.debug("GwdgPriceEstimator - computeVmsPrice- Computing the price... ");
		double price= 0; 
		double rate = Constants.DEFAULT_PRICE_VM; //This is the default value
		
		Attribute vmType= ProviderModelTool.getAttributeByName(vm, "vmType"); 
		
		if(vmType==null)
			vmType= ProviderModelTool.getAttributeByName(vm, "VMType"); 
		
			
			logger.debug("GwdgPriceEstimator - computeVmsPrice- Computing the price for vm: "+((EnumerateValue)vmType.getValue()).getName());
		
		if(vmsMap.get(((EnumerateValue)vmType.getValue()).getName())!=null)
		{	
			rate= vmsMap.get(((EnumerateValue)vmType.getValue()).getName()); 
		}
		else
			logger.warn("GwdgPriceEstimator - computeVmsPrice- The rate for vm: "+((EnumerateValue)vmType.getValue()).getName()+ "cannot be found. The default value will be used");

		price =rate*vm.getFeatureCardinality().getValue(); 
		
		return price; 
		
	}
	
	
	/** 
	 * Format of the input: 
	 * comments
	 * vmSize;price
	 * # Indicates comments
	 * Example:	
			M1.MICRO;0.070
			M1.TINY;0.080
			M1.SMALL;0.090
			M1.MEDIUM;0.100
			M1.LARGE;0.110
			M1.XLARGE;0.120
			M1.XXLARGE;0.130
			M2.SMALL;0.135
			M2.MEDIUM;0.140
			M2.LARGE;0.145
			M2.XLARGE;0.150
			C1.SMALL;0.155
			C1.MEDIUM;0.160
			C1.LARGE;0.165
			C1.XLARGE;0.170
			C1.XXLARGE;0.175
	 **/
	public void loadLocationRates(BufferedReader br)
	{
		try 
		{
			
			String line= br.readLine(); 
			
			
			logger.debug("GwdgPriceEstimationTool - loadLocationRates - processing line "+line);
			
			if(!line.startsWith("#"))
			{
				String[] infos= line.split(ProviderModelTool.LINE_INFOS_SEPARATOR); 
				
				if(infos.length==2)
				{
					vmsMap.put(infos[0], Double.parseDouble(infos[1])); 
					
				}
				else
					logger.error("GwdgPriceEstimationTool - loadLocationRates - The line "+line+" does not have the correct format. The price will be not loaded!"); 
			}
			
			
			
			
		} 
		catch (IOException e) 
		{
			
			e.printStackTrace();
		} 
		
	}
	
	

	public double estimatePricePerHour(ProviderModel fm) 
	{
		
		return estimatePrice(fm);
	}

	public double estimatePricePerMonth(ProviderModel fm) 
	{
		
		return estimatePrice(fm)*24*30;
	}

	public double estimatePricePerYear(ProviderModel fm) 
	{
		return estimatePricePerMonth(fm)*12;
	}

}
